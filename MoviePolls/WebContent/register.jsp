<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${initParam.siteTitle}</title>
<script>
		function validateForm() {
				    
		    if (form.email.value == null || form.email.value == "") {
		        alert("Error: You must enter your e-mail address");
		        form.email.focus();
		        return false;
		    }
		   
		    var atPos = form.email.value.indexOf("@");
		    var dotPos = form.email.value.lastIndexOf(".");
		    if (atPos < 1 || dotPos < atPos + 2 || dotPos + 2 >= form.email.value.length) {
		    	alert("Error: Not a valid e-mail address");
		    	form.email.focus();
		        return false;
		    }

		    if(form.pwd1.value != "" && form.pwd1.value == form.pwd2.value) {
		      if(form.pwd1.value.length < 6) {
		        alert("Error: Password must contain at least six characters!");
		        form.pwd1.focus();
		        return false;
		      }
		      if(form.pwd1.value == form.email.value) {
		        alert("Error: Password must be different from e-mail!");
		        form.pwd1.focus();
		        return false;
		      }
		      re = /[0-9]/;
		      if(!re.test(form.pwd1.value)) {
		        alert("Error: password must contain at least one number (0-9)!");
		        form.pwd1.focus();
		        return false;
		      }
		      re = /[a-z]/;
		      if(!re.test(form.pwd1.value)) {
		        alert("Error: password must contain at least one lowercase letter (a-z)!");
		        form.pwd1.focus();
		        return false;
		      }
		      re = /[A-Z]/;
		      if(!re.test(form.pwd1.value)) {
		        alert("Error: password must contain at least one uppercase letter (A-Z)!");
		        form.pwd1.focus();
		        return false;
		      }
		    } else {
		      alert("Error: Please check that you've entered and confirmed your password!");
		      form.pwd1.focus();
		      return false;
		    }
		    return true;
		  }
	</script>
</head>
<body>
	<div style="width: 800px; margin: 0 auto;">
		<jsp:include page="header.jsp" />
		Register new user<br /> <br />
		<form action="register.do" method="post" name="form"
			onsubmit="return validateForm()">
			<table>
				<tr>
					<td>E-mail address:</td>
					<td><input type="text" name="email" /></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type="password" name="pwd1" /></td>
				</tr>
				<tr>
					<td>Confirm password:</td>
					<td><input type="password" name="pwd2" /></td>
				</tr>
			</table>
			<br /> <input type="submit" value="Register" />
		</form>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>