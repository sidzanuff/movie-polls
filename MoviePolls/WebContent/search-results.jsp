<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${initParam.siteTitle}</title>
</head>
<body>
	<div style="width: 800px; margin: 0 auto;">
		<jsp:include page="header.jsp" />
		<jsp:include page="login.jsp" />
		<hr />
		Genre: ${genre.name}
		<hr />
		Search results:<br />
		<br />
		<form action="add-movies.do" method="post">
			<table style="width: 100%;">
				<tr>
					<th>Title</th>
					<th>Year</th>
					<th>Type</th>
				</tr>
				<c:forEach items="${movies}" var="movie">
					<tr>
						<td><a href="http://www.imdb.com/title/${movie.imdbID}"
							target="_blank">${movie.title}</a></td>
						<td align="center">${movie.year}</td>
						<td align="center">${movie.type }</td>
						<td><c:if test="${!empty user}">
								<input type="checkbox" name="movieId" value="${movie.imdbID}" />
							</c:if></td>
					</tr>
				</c:forEach>
			</table>
			<br />
			<c:if test="${!empty user}">
				<input type="submit" value="Add movies" />
			</c:if>
		</form>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>