<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${empty user}">
		<form action="login.do" method="post">
			E-mail: <input type="text" name="email" /> Password: <input
				type="password" name="password" /> <input type="submit"
				value="Login" />
		</form>
		<br />
		<c:url value="https://www.facebook.com/dialog/oauth" var="fbLoginUrl">
			<c:param name="client_id" value="${initParam.fbAppId}" />
			<c:param name="redirect_uri"
				value="http://circumsolar.duckdns.org/MoviePolls/facebook-login" />
		</c:url>
		<a href="register.jsp">Register</a> | <a href="${fbLoginUrl}">Login
			via Facebook</a>
	</c:when>
	<c:otherwise>
		Logged in as 		
		<c:choose>
			<c:when test="${empty user.name}">
				${user.email}
			</c:when>
			<c:otherwise>
				${user.name}
			</c:otherwise>
		</c:choose>
		<br />
		<br />
		<form action="logout.do">
			<input type="submit" value="Logout" />
		</form>
	</c:otherwise>
</c:choose>