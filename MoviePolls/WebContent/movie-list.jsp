<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${initParam.siteTitle}</title>
</head>
<body>
	<div style="width: 800px; margin: 0 auto;">
		<jsp:include page="header.jsp" />
		<jsp:include page="login.jsp" />
		<hr />
		<jsp:include page="genre.jsp" />
		<hr />
		<form action="update-votes.do" method="post">
			<table style="width: 100%;">
				<tr>
					<c:url value="index.jsp" var="orderbyTitle">
						<c:param name="orderby" value="title" />
					</c:url>
					<th align="left"><a href="${orderbyTitle}">Title</a></th>

					<c:url value="index.jsp" var="orderbyYear">
						<c:param name="orderby" value="year" />
					</c:url>
					<th><a href="${orderbyYear}">Year</a></th>

					<c:url value="index.jsp" var="orderbyType">
						<c:param name="orderby" value="type" />
					</c:url>
					<th><a href="${orderbyType}">Type</a></th>

					<c:url value="index.jsp" var="orderbyVotes">
						<c:param name="orderby" value="votes" />
					</c:url>
					<th><a href="${orderbyVotes}">Votes</a></th>

					<c:if test="${!empty user}">
						<c:url value="index.jsp" var="orderbyMyvote">
							<c:param name="orderby" value="myvote" />
						</c:url>
						<th><a href="${orderbyMyvote}">My vote</a></th>
					</c:if>
				</tr>
				<c:forEach items="${movies}" var="movie">
					<tr>
						<td><a href="http://www.imdb.com/title/${movie.imdbID}"
							target="_blank">${movie.title}</a></td>
						<td align="center">${movie.year}</td>
						<td align="center">${movie.type }</td>
						<td align="center">${movie.votes}</td>
						<c:if test="${!empty user}">
							<td align="center"><input type="checkbox" name="movieId"
								value="${movie.id}" ${movie.voted  ? 'checked' : ''} /></td>
						</c:if>
					</tr>
				</c:forEach>
			</table>
			<br />
			<c:if test="${!empty user}">
				<input type="submit" value="Update votes" />
			</c:if>
		</form>
		<c:if test="${!empty user}">
			<hr />
			<form action="find-movie">
				Add movie: <input type="text" name="movieTitle" /> <input
					type="submit" value="Find" />
			</form>
		</c:if>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>