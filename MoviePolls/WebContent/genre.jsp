<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="index.jsp">
	Genre: <select name="genreid">
		<option selected value="${genre.id}">${genre.name}</option>
		<c:forEach var="item" items="${genres}">
			<option value="${item.id}">${item.name}</option>
		</c:forEach>
	</select> <input type="submit" value="Select" />
	<c:if test="${!empty user}">
		<c:url value="add-genre.jsp" var="addGenreUrl" />
		<br />
		<br />
		<a href="${addGenreUrl}">Add new genre</a>
	</c:if>
</form>
