<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${initParam.siteTitle}</title>
</head>
<body>
	<div style="width: 800px; margin: 0 auto;">
		<jsp:include page="header.jsp" />
		<jsp:include page="login.jsp" />
		<hr />
		<b>Movie description:</b><br />
		<br />
		<table>
			<tr valign="top">
				<td width="200px">Title:</td>
				<td><b>${movie.title}</b></td>
			</tr>
			<tr>
				<td></td>
				<td><c:url value="Poster" var="imgUrl">
						<c:param name="movieId" value="${movie.imdbID}" />
					</c:url> <img src="${imgUrl}" />
			</tr>
			<tr>
				<td></td>
				<td><a href="http://www.imdb.com/title/${movie.imdbID}"
					target="_blank">IMDB page</a></td>
			</tr>
			<tr valign="top">
				<td>Year:</td>
				<td>${movie.year}</td>
			</tr>
			<tr valign="top">
				<td>Rated:</td>
				<td>${movie.rated}</td>
			</tr>
			<tr valign="top">
				<td>Released:</td>
				<td>${movie.released}</td>
			</tr>
			<tr valign="top">
				<td>Runtime:</td>
				<td>${movie.runtime}</td>
			</tr>
			<tr valign="top">
				<td>Genre:</td>
				<td>${movie.genre}</td>
			</tr>
			<tr valign="top">
				<td>Director:</td>
				<td>${movie.director}</td>
			</tr>
			<tr valign="top">
				<td>Writer:</td>
				<td>${movie.writer}</td>
			</tr>
			<tr valign="top">
				<td>Actors:</td>
				<td>${movie.actors}</td>
			</tr>
			<tr valign="top">
				<td>Plot:</td>
				<td>${movie.plot}</td>
			</tr>
			<tr valign="top">
				<td>Language:</td>
				<td>${movie.language}</td>
			</tr>
			<tr valign="top">
				<td>Country:</td>
				<td>${movie.country}</td>
			</tr>
			<tr valign="top">
				<td>Awards:</td>
				<td>${movie.awards}</td>
			</tr>
			<tr valign="top">
				<td>Metascore:</td>
				<td>${movie.metascore}</td>
			</tr>
			<tr valign="top">
				<td>Imdb Rating:</td>
				<td>${movie.imdbRating}</td>
			</tr>
			<tr valign="top">
				<td>Imdb Votes:</td>
				<td>${movie.imdbVotes}</td>
			</tr>
			<tr valign="top">
				<td>Type:</td>
				<td>${movie.type}</td>
			</tr>
		</table>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>