<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${initParam.siteTitle}</title>
</head>
<body>
	<div style="width: 800px; margin: 0 auto;">
		<jsp:include page="header.jsp" />
		<jsp:include page="login.jsp" />
		<hr />
		Error: ${error.description}
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>