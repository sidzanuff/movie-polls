package com.circumsolar.omdb;

public class MovieComparator implements java.util.Comparator<Movie> {

	@Override
	public int compare(Movie arg0, Movie arg1) {
		int order = arg0.getTitle().compareTo(arg1.getTitle());
		if (order == 0) {
			order = arg0.getYear().compareTo(arg1.getYear());
		}
		return order;
	}

}
