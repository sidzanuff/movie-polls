package com.circumsolar.omdb;

import java.util.ArrayList;
import java.util.List;

public class SearchResult {
	public List<Movie> Search = new ArrayList<Movie>();
	public String Response;
	public String Error;
	public List<Movie> getSearch() {
		return Search;
	}
	public void setSearch(List<Movie> search) {
		this.Search = search;
	}
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		this.Response = response;
	}
	public String getError() {
		return Error;
	}
	public void setError(String error) {
		this.Error = error;
	}
}