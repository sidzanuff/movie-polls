package com.circumsolar.omdb;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

import com.google.gson.Gson;

public class OMDbApi {
	public static SearchResult findMovies(String title) throws IOException {
		URL url = new URL("http://www.omdbapi.com/?s="
				+ URLEncoder.encode(title, "UTF-8"));
		InputStream input = url.openStream();
		InputStreamReader reader = new InputStreamReader(input, "UTF-8");
		SearchResult result = new Gson().fromJson(reader, SearchResult.class);
		return result;
	}

	public static Movie getMovie(String imdbID) throws IOException {
		return getMovie(imdbID, false);
	}

	public static Movie getMovie(String movieId, boolean longPlot)
			throws IOException {
		URL url = new URL("http://www.omdbapi.com/?i="
				+ URLEncoder.encode(movieId, "UTF-8")
				+ (longPlot ? "&plot=full" : ""));
		InputStream input = url.openStream();
		InputStreamReader reader = new InputStreamReader(input, "UTF-8");
		Movie movie = new Gson().fromJson(reader, Movie.class);
		reader.close();
		input.close();
		return movie;
	}
}
