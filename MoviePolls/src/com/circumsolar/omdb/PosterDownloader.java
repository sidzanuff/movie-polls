package com.circumsolar.omdb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Poster")
public class PosterDownloader extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String omdbApiKey = request.getServletContext().getInitParameter("omdbApiKey");
		String movieId = request.getParameter("movieId");
		
		if (omdbApiKey == null || movieId == null) {
			return;
		}
		
		response.setContentType("image/jpeg");
		
		URL url = new URL("http://img.omdbapi.com//?apikey=" + 
				URLEncoder.encode(omdbApiKey, "UTF-8") + "&i=" +
				URLEncoder.encode(movieId, "UTF-8"));
		InputStream input = url.openStream();
	 	
		OutputStream output = response.getOutputStream();
		
		byte[] buffer = new byte[4096];
		int bytesRead = -1;
		
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	 	
	 	input.close();
	 	output.close();
	}
}
