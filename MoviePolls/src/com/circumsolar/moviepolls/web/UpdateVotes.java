package com.circumsolar.moviepolls.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.circumsolar.moviepolls.data.VoteDao;
import com.circumsolar.moviepolls.model.Genre;
import com.circumsolar.moviepolls.model.User;
import com.circumsolar.moviepolls.model.Vote;

@WebServlet("/update-votes.do")
public class UpdateVotes extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		if (user == null) {
			handleError(request, response, "not logged in");
			return;
		}

		Genre genre = (Genre) session.getAttribute("genre");
		if (genre == null) {
			handleError(request, response, "genre not selected");
			return;
		}

		VoteDao voteDao = new VoteDao(request.getServletContext());
		voteDao.delete(user.getId(), genre.getId());

		String[] movieIds = request.getParameterValues("movieId");
		if (movieIds != null) {
			Vote vote = new Vote();
			vote.setUserId(user.getId());
			vote.setGenreId(genre.getId());

			for (String movieId : movieIds) {
				vote.setMovieId(Integer.parseInt(movieId));
				voteDao.create(vote);
			}
		}

		response.sendRedirect(response.encodeURL("index.jsp"));
	}
}
