package com.circumsolar.moviepolls.web;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.circumsolar.moviepolls.data.UserDao;
import com.circumsolar.moviepolls.model.User;
import com.circumsolar.util.CryptUtil;
import com.circumsolar.util.SecuredPassword;

@WebServlet("/register.do")
public class Register extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("pwd1");

		UserDao userDao = new UserDao(request.getServletContext());

		User user = userDao.getByEmail(email);
		if (user != null) {
			handleError(request, response,
					"user with given e-mail address already exists!");
			return;
		}

		SecuredPassword securedPassword;

		try {
			securedPassword = CryptUtil.securePassword(password);
		} catch (NoSuchAlgorithmException e) {
			throw new ServletException(e);
		}

		user = new User();
		user.setEmail(email);
		user.setPassword(securedPassword.getPassword());
		user.setSalt(securedPassword.getSalt());

		userDao.create(user);

		HttpSession session = request.getSession();
		session.setAttribute("user", user);

		response.sendRedirect(response.encodeURL("index.jsp"));
	}
}
