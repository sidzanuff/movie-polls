package com.circumsolar.moviepolls.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.circumsolar.moviepolls.model.MoviePollsError;

public class MoviePollsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final int MOVIE_DB_LOCAL = 0;
	public static final int MOVIE_DB_OMDB = 1;

	protected int moviedb;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		String db = config.getServletContext().getInitParameter("moviedb");
		if (db != null && db.equals("omdb")) {
			moviedb = MOVIE_DB_OMDB;
		} else {
			moviedb = MOVIE_DB_LOCAL;
		}
	}

	protected void handleError(HttpServletRequest request,
			HttpServletResponse response, String description)
			throws ServletException, IOException {
		MoviePollsError error = new MoviePollsError();
		error.setDescription(description);
		request.setAttribute("error", error);
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("error.jsp");
		dispatcher.forward(request, response);
	}
}
