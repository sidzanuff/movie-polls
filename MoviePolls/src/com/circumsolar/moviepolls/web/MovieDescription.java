package com.circumsolar.moviepolls.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.circumsolar.omdb.Movie;
import com.circumsolar.omdb.OMDbApi;

@WebServlet("/movie-description")
public class MovieDescription extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String imdbID = request.getParameter("imdbid");
		Movie movie = OMDbApi.getMovie(imdbID, true);
		request.setAttribute("movie", movie);
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("movie-descritpion.jsp");
		dispatcher.forward(request, response);
	}
}
