package com.circumsolar.moviepolls.web;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.circumsolar.moviepolls.data.GenreDao;
import com.circumsolar.moviepolls.data.MovieDao;
import com.circumsolar.moviepolls.data.VoteDao;
import com.circumsolar.moviepolls.model.Genre;
import com.circumsolar.moviepolls.model.MoviePollsMovie;
import com.circumsolar.moviepolls.model.User;
import com.circumsolar.moviepolls.model.Vote;

@WebServlet("/movie-list")
public class MovieList extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		GenreDao genreDao = new GenreDao(request.getServletContext());
		List<Genre> genres = genreDao.getAll();
		request.setAttribute("genres", genres);

		// get genre from request
		Genre genre = null;
		if (request.getParameter("genreid") != null) {
			int genreid;

			try {
				genreid = Integer.parseInt(request.getParameter("genreid"));
			} catch (NumberFormatException e) {
				handleError(request, response, "invalid genre id");
				return;
			}

			for (Genre g : genres) {
				if (g.getId() == genreid) {
					genre = g;
					break;
				}
			}
		}

		// get genre from session
		if (genre == null) {
			genre = (Genre) session.getAttribute("genre");
		}

		// get first genre from database
		if (genre == null) {
			genre = genres.get(0);
		}

		Collections.sort(genres, new Comparator<Genre>() {
			@Override
			public int compare(Genre o1, Genre o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		session.setAttribute("genre", genre);

		MovieDao movieDao = new MovieDao(request.getServletContext());
		List<MoviePollsMovie> movies = movieDao.getByGenre(genre.getId());

		User user = (User) session.getAttribute("user");
		if (user != null) {
			VoteDao voteDao = new VoteDao(request.getServletContext());
			List<Vote> votes = voteDao
					.getUserVotes(user.getId(), genre.getId());
			for (MoviePollsMovie movie : movies) {
				for (Vote vote : votes) {
					if (movie.getId() == vote.getMovieId()) {
						movie.setVoted(true);
						continue;
					}
				}
			}
		}

		final String orderby = request.getParameter("orderby");

		Collections.sort(movies, new Comparator<MoviePollsMovie>() {
			@Override
			public int compare(MoviePollsMovie arg0, MoviePollsMovie arg1) {
				int order = 0;

				if (orderby == null || orderby.equals("votes")) {
					order = arg1.getVotes() - arg0.getVotes();
				} else if (orderby.equals("title")) {
					order = arg0.getTitle().compareTo(arg1.getTitle());
				} else if (orderby.equals("year")) {
					order = arg1.getYear().compareTo(arg0.getYear());
				} else if (orderby.equals("type")) {
					order = arg0.getType().compareTo(arg1.getType());
				} else if (orderby.equals("myvote")) {
					order = arg1.isVoted() == arg0.isVoted() ? 0 : arg1
							.isVoted() ? 1 : -1;
				}

				if (order == 0) {
					order = arg0.getTitle().compareTo(arg1.getTitle());
				}

				if (order == 0) {
					order = arg1.getYear().compareTo(arg0.getYear());
				}

				return order;
			}
		});

		request.setAttribute("movies", movies);
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("movie-list.jsp");
		dispatcher.forward(request, response);
	}
}
