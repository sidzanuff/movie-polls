package com.circumsolar.moviepolls.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.text.WordUtils;

import com.circumsolar.moviepolls.data.GenreDao;
import com.circumsolar.moviepolls.model.Genre;
import com.circumsolar.moviepolls.model.User;

@WebServlet("/add-genre.do")
public class AddGenre extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		if (user == null) {
			handleError(request, response, "not logged in");
			return;
		}
		
		String name = request.getParameter("name").toLowerCase().trim();
		String capitalizedName = WordUtils.capitalizeFully(name);
		
		GenreDao genreDao = new GenreDao(request.getServletContext());
		List<Genre> genres = genreDao.getAll();
		
		for (Genre genre : genres) {
			if (genre.getName().toLowerCase().equals(name)) {
				handleError(request, response, "genre " + capitalizedName + " already exists");
				return;
			}
		}
		
		Genre genre = new Genre();
		genre.setName(capitalizedName);
		genreDao.create(genre);

		session.setAttribute("genre", genre);
		
		response.sendRedirect(response.encodeURL("index.jsp"));
	}
}
