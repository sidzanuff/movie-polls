package com.circumsolar.moviepolls.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.circumsolar.moviepolls.data.UserDao;
import com.circumsolar.moviepolls.model.User;
import com.circumsolar.util.CryptUtil;

@WebServlet("/login.do")
public class Login extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		UserDao userDao = new UserDao(request.getServletContext());
		User user = userDao.getByEmail(email);
		
		if (user == null) {
			handleError(request, response, "user not found");
			return;
		}
		
		if (!CryptUtil.validatePassword(password, user.getPassword(), user.getSalt())) {
			handleError(request, response, "invalid password");
			return;
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		response.sendRedirect(response.encodeURL("index.jsp"));
	}

}
