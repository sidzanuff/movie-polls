package com.circumsolar.moviepolls.web;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.circumsolar.moviepolls.data.OmdbDao;
import com.circumsolar.omdb.Movie;
import com.circumsolar.omdb.MovieComparator;
import com.circumsolar.omdb.OMDbApi;
import com.circumsolar.omdb.SearchResult;

@WebServlet("/find-movie")
public class FindMovie extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("movieTitle");

		if (title == null || title.length() < 2) {
			handleError(request, response, "title too short");
			return;
		}

		OmdbDao omdbDao = new OmdbDao(request.getServletContext());
		List<Movie> movies;
		if (moviedb == MOVIE_DB_OMDB) {
			SearchResult result = OMDbApi.findMovies(title);
			if (result.getResponse() != null && result.getResponse().equals("False"))
			{
				movies = omdbDao.findMovies(title);
			} else {
				movies = result.getSearch();
			}
		} else {
			movies = omdbDao.findMovies(title);
		}

		Collections.sort(movies, new MovieComparator());
		
		request.setAttribute("movies", movies);
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("search-results.jsp");
		dispatcher.forward(request, response);
	}
}
