package com.circumsolar.moviepolls.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.circumsolar.moviepolls.data.MovieDao;
import com.circumsolar.moviepolls.data.OmdbDao;
import com.circumsolar.moviepolls.data.VoteDao;
import com.circumsolar.moviepolls.model.Genre;
import com.circumsolar.moviepolls.model.MoviePollsMovie;
import com.circumsolar.moviepolls.model.User;
import com.circumsolar.moviepolls.model.Vote;
import com.circumsolar.omdb.Movie;
import com.circumsolar.omdb.OMDbApi;

@WebServlet("/add-movies.do")
public class AddMovies extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		if (user == null) {
			handleError(request, response, "not logged in");
			return;
		}

		Genre genre = (Genre) session.getAttribute("genre");
		if (genre == null) {
			handleError(request, response, "genre not selected");
			return;
		}

		String[] movieIds = request.getParameterValues("movieId");
		if (movieIds == null || movieIds.length == 0) {
			handleError(request, response, "no movies selected");
			return;
		}

		VoteDao voteDao = new VoteDao(request.getServletContext());
		MovieDao movieDao = new MovieDao(request.getServletContext());
		OmdbDao omdbDao = new OmdbDao(request.getServletContext());

		List<Vote> votes = voteDao.getUserVotes(user.getId(), genre.getId());

		for (String imdbID : movieIds) {
			MoviePollsMovie movie = movieDao.getMovie(imdbID);

			if (movie == null) {
				Movie imdbMovie;
				if (moviedb == MOVIE_DB_OMDB) {
					imdbMovie = OMDbApi.getMovie(imdbID);
				} else {
					imdbMovie = omdbDao.getMovie(imdbID);
				}

				if (imdbMovie == null) {
					handleError(request, response,
							"could not find movie by id " + imdbID);
					return;
				}

				movie = new MoviePollsMovie();
				movie.setImdbID(imdbID);
				movie.setTitle(imdbMovie.getTitle());
				movie.setYear(imdbMovie.getYear());
				movie.setType(imdbMovie.getType());
				movieDao.create(movie);
			}

			if (!isVoted(votes, movie.getId(), genre.getId())) {
				Vote vote = new Vote();
				vote.setMovieId(movie.getId());
				vote.setUserId(user.getId());
				vote.setGenreId(genre.getId());
				voteDao.create(vote);
			}
		}

		response.sendRedirect(response.encodeURL("index.jsp"));
	}

	private static boolean isVoted(List<Vote> votes, int movieId, int genereId) {
		for (Vote vote : votes) {
			if (vote.getMovieId() == movieId && vote.getGenreId() == genereId) {
				return true;
			}
		}
		return false;
	}
}
