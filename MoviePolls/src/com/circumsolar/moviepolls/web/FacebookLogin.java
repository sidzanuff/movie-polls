package com.circumsolar.moviepolls.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.circumsolar.moviepolls.data.UserDao;
import com.circumsolar.moviepolls.model.FacebookUserData;
import com.circumsolar.moviepolls.model.User;
import com.google.gson.Gson;

@WebServlet("/facebook-login")
public class FacebookLogin extends MoviePollsServlet {
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String code = request.getParameter("code");
		if (code == null || code.equals("")) {
			handleError(request, response, "login failed");
			return;
		}

		String token = null;
		try {
			String url = "https://graph.facebook.com/oauth/access_token?client_id="
					+ request.getServletContext().getInitParameter("fbAppId")
					+ "&redirect_uri="					
					+ URLEncoder.encode("http://circumsolar.duckdns.org/MoviePolls/facebook-login", "UTF-8")
					+ "&client_secret="
					+ request.getServletContext().getInitParameter("fbSecret")
					+ "&code=" + code;

			token = download(url);
			
			if (token.startsWith("{")) {
				throw new Exception("error on requesting token: " + token + " with code: " + code);
			}
		} catch (Exception e) {
			handleError(request, response, "login failed");
		}

		String graph = download("https://graph.facebook.com/me?" + token);
		FacebookUserData fbUser = new Gson().fromJson(graph, FacebookUserData.class);
		
		UserDao userDao = new UserDao(request.getServletContext());
		User user = userDao.getByFacebookId(fbUser.getId());
		
		if (user == null) {
			user = new User();
			
			String name = fbUser.getFirst_name();
			if (fbUser.getMiddle_name() != null && !fbUser.getMiddle_name().isEmpty()) {
				name = name + " " + fbUser.getMiddle_name();
			}
			if (fbUser.getLast_name() != null && !fbUser.getLast_name().isEmpty()) {
				name = name + " " + fbUser.getLast_name();
			}
			
			user.setName(name);
			user.setFacebookId(fbUser.getId());
			userDao.create(user);
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		
		response.sendRedirect(response.encodeURL("index.jsp"));
	}
	
	private static final String download(String url) throws MalformedURLException, IOException {
		URLConnection connection = new URL(url).openConnection();
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
		StringBuffer buffer = new StringBuffer();
		while ((line = reader.readLine()) != null) {
			buffer.append(line + "\n");
		}
		reader.close();
		return buffer.toString();
	}
}
