package com.circumsolar.moviepolls.model;

import com.circumsolar.omdb.Movie;

public class MoviePollsMovie extends Movie {
	public int id;
	public int votes;
	public boolean voted;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVotes() {
		return votes;
	}
	public void setVotes(int votes) {
		this.votes = votes;
	}
	public boolean isVoted() {
		return voted;
	}
	public void setVoted(boolean voted) {
		this.voted = voted;
	}
}
