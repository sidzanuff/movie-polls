package com.circumsolar.moviepolls.model;

public class MoviePollsError {
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
