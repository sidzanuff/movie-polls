package com.circumsolar.moviepolls.model;

public class Vote {
	public int id;
	public int userId;
	public int movieId;
	public int genreId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genereId) {
		this.genreId = genereId;
	}
}
