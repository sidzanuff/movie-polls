package com.circumsolar.moviepolls.data;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.circumsolar.moviepolls.model.Genre;

public class GenreDao extends DaoBase {

	public GenreDao(ServletContext context) {
		super(context);
	}

	public void create(Genre genre) throws ServletException {
		String query = "insert into generes (name) values (?)";
		prepareStatement(query);
		setString(1, genre.getName());
		int id = finalizeUpdate();
		genre.setId(id);
	}
	
	public List<Genre> getAll() throws ServletException {
		String query = "select id, name from generes";
		prepareStatement(query);
		fireQuery();
		List<Genre> genres = new ArrayList<Genre>();
		while(next()) {
			Genre genre = new Genre();
			genre.setId(getInt("id"));
			genre.setName(getString("name"));
			genres.add(genre);
		}
		closeConnection();
		return genres;
	}
	
	public Genre getGenre(int genreId) throws ServletException {
		String query = "select name from generes where id = ?";
		prepareStatement(query);
		setInt(1, genreId);
		fireQuery();
		Genre genre = null;
		if (next()) {
			genre = new Genre();
			genre.setId(genreId);
			genre.setName(getString("name"));
		}
		closeConnection();
		return genre;
	}
}
