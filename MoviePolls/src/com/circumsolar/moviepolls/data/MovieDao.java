package com.circumsolar.moviepolls.data;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.circumsolar.moviepolls.model.MoviePollsMovie;

public class MovieDao extends DaoBase {

	public MovieDao(ServletContext context) {
		super(context);
	}

	public void create(MoviePollsMovie movie) throws ServletException {
		String query = "insert into movies(imdbid, title, year, type) values (?, ?, ?, ?)";
		prepareStatement(query);
		setString(1, movie.getImdbID());
		setString(2, movie.getTitle());
		setString(3, movie.getYear());
		setString(4, movie.getType());
		int id = finalizeUpdate();
		movie.setId(id);
	}

	public List<MoviePollsMovie> getAll() throws ServletException {
		String query = "select "
				+ "movies.id as id, "
				+ "movies.imdbid as imdbid, "
				+ "movies.title as title, "
				+ "movies.year as year, "
				+ "movies.type as type, "
				+ "count(votes.id) as votes "
				+ "from movies left join votes on movies.id=votes.movieid group by movies.id";
		prepareStatement(query);
		fireQuery();
		List<MoviePollsMovie> movies = new ArrayList<MoviePollsMovie>();
		while (next()) {
			MoviePollsMovie movie = readMovie();
			movies.add(movie);
		}
		closeConnection();
		return movies;
	}

	public List<MoviePollsMovie> getByGenre(int genreId)
			throws ServletException {
		String query = "select " + "movies.id as id, "
				+ "movies.imdbid as imdbid, " + "movies.title as title, "
				+ "movies.year as year, " + "movies.type as type, "
				+ "count(votes.id) as votes "
				+ "from movies left join votes on movies.id=votes.movieid "
				+ "where votes.genereid=? " + "group by movies.id";
		prepareStatement(query);
		setInt(1, genreId);
		fireQuery();
		List<MoviePollsMovie> movies = new ArrayList<MoviePollsMovie>();
		while (next()) {
			MoviePollsMovie movie = readMovie();
			movies.add(movie);
		}
		closeConnection();
		return movies;
	}

	public MoviePollsMovie getMovie(String imdbID) throws ServletException {
		String query = "select " + "movies.id as id, "
				+ "movies.imdbid as imdbid, " + "movies.title as title, "
				+ "movies.year as year, " + "movies.type as type, "
				+ "count(votes.id) as votes "
				+ "from movies left join votes on movies.id=votes.movieid "
				+ "where movies.imdbid=? " + "group by movies.id";
		prepareStatement(query);
		setString(1, imdbID);
		fireQuery();
		MoviePollsMovie movie = null;
		if (next()) {
			movie = readMovie();
		}
		closeConnection();
		return movie;
	}

	private MoviePollsMovie readMovie() throws ServletException {
		MoviePollsMovie movie = new MoviePollsMovie();
		movie.setId(getInt("id"));
		movie.setImdbID(getString("imdbid"));
		movie.setTitle(getString("title"));
		movie.setYear(getString("year"));
		movie.setType(getString("type"));
		movie.setVotes(getInt("votes"));
		return movie;
	}
}
