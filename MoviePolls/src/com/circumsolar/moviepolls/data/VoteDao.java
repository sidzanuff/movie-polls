package com.circumsolar.moviepolls.data;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.circumsolar.moviepolls.model.Vote;

public class VoteDao extends DaoBase {

	public VoteDao(ServletContext context) {
		super(context);
	}

	public void create(Vote vote) throws ServletException {
		String query = "insert into votes (userid, movieid, genereid) values (?, ?, ?)";
		prepareStatement(query);
		setInt(1, vote.getUserId());
		setInt(2, vote.getMovieId());
		setInt(3, vote.getGenreId());
		int id = finalizeUpdate();
		vote.setId(id);
	}
	
	public List<Vote> getUserVotes(int userId, int genreId) throws ServletException {
		String query = "select id, userid, movieid, genereid from votes where userid=? and genereid=?";
		prepareStatement(query);
		setInt(1, userId);
		setInt(2, genreId);
		fireQuery();
		List<Vote> votes = new ArrayList<Vote>();
		while(next()) {
			Vote vote = new Vote();
			vote.setId(getInt("id"));
			vote.setUserId(getInt("userid"));
			vote.setMovieId(getInt("movieid"));
			vote.setGenreId(getInt("genereid"));
			votes.add(vote);
		}
		closeConnection();
		return votes;
	}
	
	public void delete(int userId, int genereId) throws ServletException {
		String query = "delete from votes where userid=? and genereid=?";
		prepareStatement(query);
		setInt(1, userId);
		setInt(2, genereId);
		finalizeUpdate();
	}
}
