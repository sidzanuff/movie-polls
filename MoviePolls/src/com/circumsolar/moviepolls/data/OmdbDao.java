package com.circumsolar.moviepolls.data;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.circumsolar.omdb.Movie;

public class OmdbDao extends DaoBase {

	public OmdbDao(ServletContext context) {
		super(context);
	}

	public List<Movie> findMovies(String title) throws ServletException {
		// String query =
		// "select imdbid, title, year, type from omdb where title REGEXP ?";
		// prepareStatement(query);
		// setString(1, "[[:<:]]" + title + "[[:>:]]");

		String query = "select imdbid, title, year, type from omdb where title like ?";
		prepareStatement(query);
		setString(1, "%" + title + "%");

		fireQuery();

		String regex = "\\b" + title.toLowerCase() + "\\b";
		Pattern pattern = Pattern.compile(regex);

		List<Movie> movies = new ArrayList<Movie>();
		while (next()) {
			Matcher matcher = pattern.matcher(getString("title").toLowerCase());
			if (matcher.find()) {
				Movie movie = new Movie();
				movie.setImdbID(getString("imdbid"));
				movie.setTitle(getString("title"));
				movie.setYear(getString("year"));
				movie.setType(getString("type"));
				movies.add(movie);
			}
		}
		closeConnection();

		return movies;
	}

	public Movie getMovie(String imdbID) throws ServletException {
		String query = "select imdbid, title, year, type from omdb where imdbid=?";
		prepareStatement(query);
		setString(1, imdbID);
		fireQuery();
		Movie movie = null;
		if (next()) {
			movie = new Movie();
			movie.setImdbID(getString("imdbid"));
			movie.setTitle(getString("title"));
			movie.setYear(getString("year"));
			movie.setType(getString("type"));
		}
		closeConnection();
		return movie;
	}
}
