package com.circumsolar.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class CryptUtil {
	
	public static SecuredPassword securePassword(String password) throws NoSuchAlgorithmException
    {
        String salt = getSalt();
        String hashedPassword = hashPassword(password, salt);
        SecuredPassword securedPassword = new SecuredPassword();
        securedPassword.setSalt(salt);
        securedPassword.setPassword(hashedPassword);
        return securedPassword;
    }
	
	public static boolean validatePassword(String password, String hashedPassword, String salt) {
		return hashPassword(password, salt).equals(hashedPassword);
	}

	private static String hashPassword(String password, String salt) {
		String hashedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt.getBytes());
            byte[] bytes = md.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            hashedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashedPassword;
	}
	
    private static String getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
    	return new BigInteger(130, sr).toString(32);
    }
}
